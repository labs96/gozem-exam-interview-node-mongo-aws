FROM node:18.18.2-slim

# Install system dependencies
RUN apt-get update && apt-get install -y \
    git \
    curl \
    vim

# Set working directory
WORKDIR /usr/src/app

COPY package*.json /usr/src/app

RUN npm install -g npm@10.2.3  && \
    npm install

COPY . .

#CMD ["node", "--watch", "src/server.js"]
#CMD ["npm", "run", "dev"]
