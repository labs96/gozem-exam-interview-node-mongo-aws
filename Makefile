#######################################
# MAKEFILE-PROJECT OPTIONS
#######################################

node_version:=$(shell node -v)
npm_version:=$(shell npm -v)
timeStamp:=$(shell date +%Y%m%d%H%M%S)

DIR_PRJ_ROOT :=.
USE_GLOBAL_COMPILE :=
USE_GLOBAL_PLUGIN_COMPILE :=

.PHONY: show up down clean i

show:
	@ echo Timestamp: "$(timeStamp)"
	@ echo Node Version: $(node_version)
	@ echo npm_version: $(npm_version)

dev:
	@ docker compose --env-file .env -f docker-compose.local.yml up -d

dev-build:
	@ docker compose --env-file .env -f docker-compose.local.yml up -d --build

down:
	@ docker compose -f docker-compose.local.yml down

api:
	@ docker compose -f docker-compose.local.yml exec api-g bash

mongodb:
	@ docker compose -f docker-compose.local.yml exec mongodb-g bash

clean:
	echo "cleaning the dist directory"
	@ rm -rf dist
