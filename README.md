# Gozem Labs

This project was created by Gozem Team / Yannick NKENDEM

## Before Start

<p>add this lines in the file /etc/hosts in your local computer</p>
<span>127.0.0.1   gozem.localhost</span><br/>
<span>127.0.0.1   api.gozem.localhost</span><br/>
<span>127.0.0.1   mailpit.gozem.localhost</span><br/>
<span>127.0.0.1   storage.gozem.localhost</span><br/>

### pré requis
<ul>
    <li>Avoir Docker installer sur votre machine</li>
    <li>Avoir Git installer sur votre machine</li>
    <li>Editeur de code comme Vscode, WebStorm...</li>
    <li>Avoir Make installer sur votre machine</li>
    <ul>
        <li>Sur MacOs : RUN `brew install make`</li>
        <li>Sur Linux et Windows, telecharger depuis les sources puis installer</li>
    </ul>
</ul>

### Cloner le projet
Run `git clone `

### Mettre à jour votre fichier d'environnement </h4>
<a>cp .env.example .env</a>

### Install the Development Environment
RUN `make dev`

### Rebuild the Development Environment
RUN `make dev-build`

### Down the Development Environment
Run `make down`

### Inter-agir avec le container API
RUN `make api`

### Inter-agir avec le container Base de donnee
RUN `make mongodb`

## Create new Container Module 

Run `cd src/App`
Then `mkdir Container && cd Container`
Then `mkdir -p controllers database models routes utils services request middlewares database/data database/seeders`
