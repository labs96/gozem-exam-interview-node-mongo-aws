const mongoose = require('mongoose')

const connectDatabase = () => {
   if (process.env.API_ENV === 'local' || process.env.API_ENV === 'development') {
      mongoose.set('strictQuery', true);
      mongoose.connect(`${process.env.DB_HOST}${process.env.DB_DATABASE}`, {
         useNewUrlParser: true,
         useUnifiedTopology: true,
         // useCreateIndex: true,
      }).then(con => {
         console.log(`MongoDB Database connected with HOST: ${con.connection.host}`);
      })
   } else {
      mongoose.set('strictQuery', true);
      mongoose.connect(`${process.env.DB_HOST}${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_DATABASE}/?retryWrites=true&w=majority`, {
         useNewUrlParser: true,
         useUnifiedTopology: true,
      }).then(con => {
         console.log(`MongoDB Database connected with HOST: ${con.connection.host}`);
      })
   }
}

module.exports = connectDatabase
