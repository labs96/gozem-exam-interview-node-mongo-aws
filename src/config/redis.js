const redis = require('redis');

const redisClient = redis.createClient({
    host: `${process.env.REDIS_HOST}`,
    port: `${process.env.REDIS_PORT}`,
    password: `${process.env.REDIS_PASSWORD}`,
    legacyMode: true
});

// redisClient.connect();

redisClient.on('error', function (err) {
    console.log('Could not establish a connection with redis. ' + err);
});

redisClient.on('connect', function (err) {
    console.log('Connected to redis successfully');
});

module.exports = redisClient;


