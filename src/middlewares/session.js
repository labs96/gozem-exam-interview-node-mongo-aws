const session = require('express-session');
const RedisStore = require('connect-redis').default;
const redisClient = require('../config/redis');

// Initialize store.
let redisStore = new RedisStore({
    client: redisClient,
    prefix: `${process.env.API_DISPLAY_NAME}` ?? 'latopel',
})

module.exports = session({
    store: redisStore,
    secret: 'Secret@@@1234',
    saveUninitialized: false,
    resave: false,
    name: 'latopel',
    cookie: {
        secure: `${process.env.API_ENV}` == 'local' ? false : true,
        // secure: false, // if true: only transmit cookie over https, in prod, always activate this
        httpOnly: false, // if true: prevents client side JS from reading the cookie
        maxAge: process.env.COOKIE_EXPIRES_TIME * 24 * 60 * 60 * 1000, // session max age in milliseconds
        // explicitly set cookie to lax
        // to make sure that all cookies accept it
        // you should never use none anyway
        sameSite: 'lax',
    },
});
