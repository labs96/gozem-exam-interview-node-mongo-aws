const express = require('express');
const router = express.Router();

const {
    getAllRoles,
    findRoleById,
    createRole,
    updateRoleById,
    deleteRoleById
} = require('./../controllers/roleController')

const { isAuthenticatedUser, authorizeRoles } = require('../../User/middlewares/auth')

// Admin routes

router.route('/admin/roles').get(isAuthenticatedUser, authorizeRoles( 'admin'), getAllRoles);
router.route('/admin/role/:id').get(isAuthenticatedUser, authorizeRoles( 'admin'), findRoleById);

router.route('/admin/role/new').post(isAuthenticatedUser, authorizeRoles('admin'), createRole);

router.route('/admin/role/:id').put(isAuthenticatedUser, authorizeRoles('admin'), updateRoleById)
                               .delete(isAuthenticatedUser, authorizeRoles('admin'), deleteRoleById);


module.exports = router;
