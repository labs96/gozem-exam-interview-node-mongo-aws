const mongoose = require('mongoose')

const roleSchema = new mongoose.Schema({
    name : {
        type: String,
        required : [true, 'Please enter role name'],
        unique: true,
        maxLength: [32, 'Role name cannot exceed 100 characters']
    },
    displayName : {
        type: String,
        required : [true, 'Please enter role display name'],
        trim: true,
        maxLength: [255, 'Role display name cannot exceed 100 characters']
    },
    description : {
        type: String,
    },
    level: {
        type: Number,
        required: true
    },
    isLock: {
        type: Boolean,
        required: true
    },
    isActive: {
        type: Boolean,
        default: true,
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    },
    deletedAt: {
        type: Date
    },
});

module.exports = mongoose.model('Role', roleSchema)
