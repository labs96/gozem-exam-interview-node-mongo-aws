const Role = require('../models/role')

const ErrorHandler = require('../../../utils/errorHandler')
const catchAsyncErrors = require('../../../middlewares/catchAsyncErrors');
const APIFeatures = require('../../../utils/apiFeatures');


// Admin Routes

// Get All roles => /v1/admin/roles
exports.getAllRoles = catchAsyncErrors(async (req, res, next) => {

    const resPerPage = req.query.limit || 10;
    const rolesCount = await Role.countDocuments();

    const apiFeatures = new APIFeatures(Role.find(), req.query)
                                    .search()
                                    .pagination(resPerPage);

    // const roles = await Role.find();
    const roles = await apiFeatures.query;

    res.status(200).json({
        success: true,
        count: roles.length,
        rolesCount,
        roles
    })
})

//  Get Single role details => /v1/admin/role/:id
exports.findRoleById = catchAsyncErrors(async (req, res, next) => {

    const role = await Role.findById(req.params.id);

    if (!role) {
        return next(new ErrorHandler('Role not found', 404));
    }

    res.status(200).json({
        success: true,
        role
    })
})


// create new role => /v1/admin/role/new
exports.createRole = catchAsyncErrors(async (req, res, next) => {

    const role = await Role.create(req.body);

    res.status(201).json({
        success: true,
        role
    });

})

// Update Role => /v1/admin/role/:id
exports.updateRoleById = catchAsyncErrors(async (req, res, next) => {

    let role = await Role.findById(req.params.id);

    if (!role) {
        return next(new ErrorHandler('Role not found', 404));
    }

    role = await Role.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true,
        useFindAndModify: false
    });

    res.status(200).json({
        success: true,
        role
    })
})

// Delete Role => /v1/admin/role/:id
exports.deleteRoleById = catchAsyncErrors(async (req, res, next) => {

    const role = await Role.findById(req.params.id);

    if (!role) {
        return next(new ErrorHandler('Role not found', 404));
    }

    await role.remove();

    res.status(200).json({
        success: true,
        message: 'Role is deleted'
    })
})
