const dotenv = require('dotenv');
const connectDatabase = require('../../../../config/database');

const roles = require('../data/roles');
const Role = require('../../models/role');

// Setting dotenv file
dotenv.config()

connectDatabase();

const seedRoles = async () => {
    try {
        
        await Role.deleteMany();
        console.log('All Roles are deleted');

        await Role.create(roles);
        console.log('All Roles are added');

        process.exit();

    } catch (error) {
        console.log(error.message);
        process.exit();
    }
}

seedRoles();