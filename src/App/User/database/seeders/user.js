const dotenv = require('dotenv');
const connectDatabase = require('../../../../config/database');

const users = require('../data/users');
const { connect } = require('mongoose');
const User = require('../../models/user');

// Setting dotenv file
dotenv.config()

connectDatabase();

const seedUsers = async () => {
    try {
        
        await User.deleteMany();
        console.log('Users are deleted');

        await User.create(users);
        console.log('All Users are added');

        process.exit();

    } catch (error) {
        console.log(error.message);
        process.exit();
    }
}

seedUsers();