const express = require('express');
const router = express.Router();

const {
    getUserProfile,
    updatePassword,
    updateProfile,
    getAllUsers,
    getUserDetails,
    updateUserDetails,
    deleteUser,
    createUser
} = require('../controllers/userController');

const { isAuthenticatedUser, authorizeRoles } = require('../../User/middlewares/auth')

router.route('/me').get(isAuthenticatedUser, getUserProfile);
router.route('/password/update').put(isAuthenticatedUser, updatePassword);
router.route('/me/update').put(isAuthenticatedUser, updateProfile);

// Admin routes
router.route('/admin/users').get(isAuthenticatedUser, authorizeRoles('admin','manager'), getAllUsers);

router.route('/admin/user/create').post(isAuthenticatedUser, authorizeRoles('admin','manager'), createUser);

router.route('/admin/user/:id').get(isAuthenticatedUser, authorizeRoles('admin','manager'), getUserDetails)
                                .put(isAuthenticatedUser, authorizeRoles('admin','manager'), updateUserDetails)
                                .delete(isAuthenticatedUser, authorizeRoles('admin'), deleteUser);

module.exports = router;
