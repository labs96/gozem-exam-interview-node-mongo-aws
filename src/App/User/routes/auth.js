const express = require('express');
const router = express.Router();

const { 
    checkUserByEmail,
    registerUser, 
    loginUser, 
    forgotPassword,
    resetPassword,
    logout
} = require('../controllers/auth/authController');

const { isAuthenticatedUser } = require('../middlewares/auth');

router.route('/check/user/email').post(checkUserByEmail);

router.route('/register').post(registerUser);
router.route('/login').post(loginUser);

router.route('/password/forgot').post(forgotPassword);
router.route('/password/reset/:token').put(resetPassword);

router.route('/logout').get(isAuthenticatedUser, logout);


module.exports = router;