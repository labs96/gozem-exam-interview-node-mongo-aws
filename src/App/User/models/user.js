const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');


const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        maxLength: [128, 'Your firstName cannot exceed 128 characters']
    },
    lastName: {
        type: String,
        required : [true, 'Please enter your lastName'],
        maxLength: [128, 'Your lastName cannot exceed 128 characters']
    },
    email: {
        type: String,
        required : [true, 'Please enter your email'],
        unique: true,
        maxLength: [128, 'Your email cannot exceed 128 characters'],
        validate: [validator.isEmail, 'Please enter valid email address']
    },
    phoneNumber : [
        {
            phone: {
                type: String,
                required: true,
                minLength: [9, 'Your phoneNumber must be longer than 9 characters'],
                maxLength: [32, 'Your phoneNumber cannot exceed 32 characters']
            },
            phoneCode: {
                type: String,
                required: true,
            },
            whatsApp: {
                type: Boolean,
                required: true,
            },
            verifiedAt: {
                type: Date
            }
        }
    ],
    password: {
        type: String,
        required : [true, 'Please enter your password'],
        minLength: [6, 'Your password must be longer than 6 characters'],
        select: false //don't display password of user
    },
    birthDay: {
        type: String,
        default: null
    },
    gender: {
        type: String,
        enum: {
            values: [
                'Male',
                'Female'
            ],
            message: 'Please select correct gender'
        },
    },
    address: {
        country: {
            type: String,
            default: null
        },
        city: {
            type: String,
            default: null
        },
        address_lines: {
            type: String,
            default: null
        },
        latitude: {
            type: String,
            default: null
        },
        longitude: {
            type: String,
            default: null
        },
    },
    profilePicture: {
        public_id: {
            type: String,
        },
        url: {
            type: String,
        }
    },
    role: {
        type: String,
        default: 'user'
    },
    isActive: {
        type: Boolean,
        default: false,
    },
    isBanned: {
        type: Boolean,
        default: false
    },
    createdBy: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        default: null
    },
    updatedBy: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        default: null
    },
    emailVerifiedAt: {
        type: Date,
        default: null,
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    },
    deletedAt: {
        type: Date,
        default: null
    },
    resetPasswordToken: {
        type: String,
        default: null
    },
    resetPasswordCode: {
        type: String,
        default: null
    },
    resetPasswordExpire: {
        type: String,
        default: null
    }
});

// Encrypting password before saving user
userSchema.pre('save', async function (next) {
    if (!this.isModified('password')) {
        next();
    }

    this.password = await bcrypt.hash(this.password, 10)

});

//  Compare user password
userSchema.methods.comparePassword = async function (enteredPassword) {
    return await bcrypt.compare(enteredPassword, this.password);
}

// Return JWT token
userSchema.methods.getJwtToken = function () {
    return jwt.sign({ id: this._id }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_TIME
    });
}

// Generate password reset token
userSchema.methods.getResetPasswordToken = function () {
    // Generate token
    const resetToken = crypto.randomBytes(20).toString('hex');

    // Hash and set to resetPasswordToken
    this.resetPasswordToken = crypto.createHash('sha256').update(resetToken).digest('hex');

    // Set token expire time
    this.resetPasswordExpire = Date.now() + 30 * 60 * 1000; //30 minutes

    return resetToken;
}

module.exports = mongoose.model('User', userSchema)
