const User = require('../models/user');

const ErrorHandler = require('../../../utils/errorHandler');
const catchAsyncErrors = require('../../../middlewares/catchAsyncErrors');

// Get currently logged in user details => /api/v1/me
exports.getUserProfile = catchAsyncErrors(async (req, res, next) => {

    const user = await User.findById(req.user.id);

    res.status(200).json({
        success: true,  
        user
    }) 
})

// Update / Change password  => /api/v1/password/update
exports.updatePassword = catchAsyncErrors(async (req, res, next) => {

    const user = await User.findById(req.user.id).select('+password');

    const { oldPassword, password } = req.body;

    // Check previous user password
    const isMatched = await user.comparePassword(oldPassword);

    if(!isMatched) {
        return next( new ErrorHandler('Old password is incorrect', 400));
    }

    user.password = password;
    await user.save();

    res.status(200).json({
        success: true,
        message: 'Password updated successfully'
    })
})

// Update user profile => /api/v1/me/update
exports.updateProfile = catchAsyncErrors(async (req, res, next) => {

    // Update avatar: TODO

    const user = await User.findByIdAndUpdate(req.user.id, req.body, {
        new: true,
        runValidators: true,
        useFindAndModify: false
    });

    res.status(200).json({
        success: true,
        user
    }) 

});

// Admin Routes   -------------------

// Create User => /api/v1/admin/user/create
exports.createUser = catchAsyncErrors(async (req, res, next) => {

    req.body.created_by = req.user.id;
    req.body.updated_by = req.user.id;

    const user = await User.create(req.body);

    res.status(201).json({
        success: true,
        message: 'User(s) created successfuly',
        user
    });

})

//  Get All users => /api/v1/admin/users
exports.getAllUsers = catchAsyncErrors(async (req, res, next) => {

    const users = await User.find();

    res.status(200).json({
        success: true,  
        users
    }) 
})

// Get User Details => /api/v1/admin/user/:id
exports.getUserDetails = catchAsyncErrors(async (req, res, next) => {

    const user = await User.findById(req.params.id);

    if (!user) {
        return next(new ErrorHandler('User not found', 404));
    }

    res.status(200).json({
        success: true, 
        user
    })  
})

// Update User => /api/v1/admin/user/:id
exports.updateUserDetails = catchAsyncErrors(async (req, res, next) => {

    let user = await User.findById(req.params.id);

    if (!user) {
        return next(new ErrorHandler('User not found', 404));
    }

    user = await User.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true,
        useFindAndModify: false
    });

    res.status(200).json({
        success: true, 
        user
    }) 
})

// Delete User => /api/v1/admin/user/:id
exports.deleteUser = catchAsyncErrors(async (req, res, next) => {

    const user = await User.findById(req.params.id);

    if (!user) {
        return next(new ErrorHandler('User not found', 404));
    }

    // Remove avatar from cloudinary - TODO

    await user.remove();

    res.status(200).json({
        success: true, 
        message: 'User is deleted'
    }) 
})