const User = require('../../models/user');

const ErrorHandler = require('../../../../utils/errorHandler');
const catchAsyncErrors = require('../../../../middlewares/catchAsyncErrors');
const sendToken = require('../../utils/jwtToken');
const sendEmail = require('../../../../utils/notifications/sendEmailWithSMTPCreds');
const { statusCode, message } = require('./../../../../constants/index')

const crypto = require('crypto');

// Check if email exist in user database => /api/v1/check/email
exports.checkUserByEmail = catchAsyncErrors ( async (req, res, next) => {

    const { email } = req.body;

    const user = await User.findOne({ email });

    if (!user) {
        return next( new ErrorHandler('No account associated with this email', statusCode.NOT_FOUND));
    }

    res.status(statusCode.GET_OK).json({
        success: true,
        message: 'Account with this email already exist'
    })

});

// Register a user => /api/v1/register
exports.registerUser = catchAsyncErrors( async (req, res, next) => {

    const { firstName, lastName, email, password }  = req.body;

    const user = await User.create({
        firstName,
        lastName,
        email,
        password,
        avatar: {
            public_id: 'w_200/lady',
            url: 'https://res.cloudinary.com/demo/image/upload/c_crop,g_face,h_400,w_400/r_max/c_scale,w_200/lady.jpg'
        }
    });

    const message = `Welcome to our platform ${user.firstName} ${user.lastName}`;

    await sendEmail({
        email: user.email,
        subject: `${process.env.API_DISPLAY_NAME} Welcome`,
        message
    });

    sendToken(user, statusCode.GET_OK, req, res);
});

//  Login User => /api/v1/login
exports.loginUser = catchAsyncErrors( async (req, res, next) => {

    const { email, password }  = req.body;

    // Checks if email and password is entered by user
    if (!email || !password) {
        return next( new ErrorHandler('Please enter email & password', statusCode.BAD_REQUEST));
    }

    // Finding user in database
    const user = await User.findOne({ email }).select('+password')

    if (!user) {
        return next( new ErrorHandler('Invalid Email or Password', statusCode.BAD_REQUEST));
    }

    // Checks if password is correct or not
    const isPasswordMatched = await user.comparePassword(password);

    if(!isPasswordMatched) {
        return next( new ErrorHandler('Invalid Email or Password', statusCode.BAD_REQUEST));
    }

    // check if user is active


    // store session as session storage - redis
    req.session.user = user;

    sendToken(user, statusCode.GET_OK, req, res);

});

// Forgot Password => /api/v1/password/forgot
exports.forgotPassword = catchAsyncErrors(async (req, res, next) => {

    const user = await User.findOne({ email: req.body.email });

    if(!user) {
        return next( new ErrorHandler('User not found with this email', 404));
    }

    // Get reset token
    const resetToken = user.getResetPasswordToken();

    await user.save({ validateBeforeSave: false });

    // Create reset password url
    const resetUrl = `${req.protocol}://${req.get('host')}/api/v1/password/reset/${resetToken}`;

    const message = `Your password reset token is an follow:\n\n${resetUrl}\n\nIf you have not
    requested this email, then ignore it.`;

    try {
        await sendEmail({
            email: user.email,
            subject: `${process.env.API_DISPLAY_NAME} Password Recorvery`,
            message
        });

        res.status(200).json({
            sucess: true,
            message: `Email send to ${user.email}`
        })

    }catch (error) {
        user.resetPasswordToken = undefined;
        user.resetPasswordExpire = undefined;

        await user.save({ validateBeforeSave: false });

        return next( new ErrorHandler(error.message, 500));
    }

});

// Reset Password => /api/v1/password/reset/:token
exports.resetPassword = catchAsyncErrors(async (req, res, next) => {

    // Hash URL token
    const resetPasswordToken = crypto.createHash('sha256').update(req.params.token).digest('hex');

    const user = await User.findOne({
        resetPasswordToken,
        resetPasswordExpire: { $gt: Date.now() }
    });

    if(!user) {
        return next( new ErrorHandler('Password reset token is invalid or has been expired', 400));
    }

    if(req.body.password !== req.body.confirmPassword) {
        return next( new ErrorHandler('Password does not match', 400));
    }

    // Setup new password
    user.password = req.body.password;

    user.resetPasswordToken = undefined;
    user.resetPasswordExpire = undefined;

    await user.save();

    // sendToken(user, 200, res);
    res.status(200).json({
        sucess: true,
        message: 'your password has been successfully changed'
    })

})


// Logout user => /api/v1/logout
exports.logout = catchAsyncErrors(async (req, res, next) => {

    req.session.destroy();

    res.status(200).json({
        sucess: true,
        message: 'Logged out'
    })

});
