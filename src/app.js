const express = require('express');
const app = express();
const cors = require('cors');
const session = require('./middlewares/session');
const cookieParser = require('cookie-parser');
const errorMiddleware = require('./middlewares/erros')
var os = require('os')

// enable this if you run behind a proxy (e.g. nginx)
app.set('trust proxy', 1);

// cors
app.use(cors());

app.use(express.json());
app.use(cookieParser());
app.use(session);

// Import all routes
const auth = require('./App/User/routes/auth');
const users = require('./App/User/routes/user');
const roles = require('./App/Role/routes/role');

// Welcome to latopel
app.get('/', function (req, res) {
    res.status(200).json(`[${process.env.API_DISPLAY_NAME} Entrypoint in ${os.hostname()}]`)
})

app.get(`/${process.env.API_PREFIX_V1}`, function (req, res) {
    res.status(200).json(`[${process.env.API_DISPLAY_NAME} Entrypoint (API V1) in ${os.hostname()}]`)
})

app.use(`/${process.env.API_PREFIX_V1}`, auth);
app.use(`/${process.env.API_PREFIX_V1}`, users);
app.use(`/${process.env.API_PREFIX_V1}`, roles);

// Middleware to handle errors
app.use(errorMiddleware)

module.exports = app
