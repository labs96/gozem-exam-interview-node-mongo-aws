const app = require('./app')
const connectDatabase = require('./config/database')

const dotenv = require('dotenv');

// Handle Uncaugth Exceptions
process.on('uncaughtException', err => {
    console.log(`ERROR: ${err.stack}`);
    console.log('Shutting down due to Uncaugth Exceptions');
    process.exit(1);
})

// Setting up config file
dotenv.config()

// Connecting to database MongoDB
connectDatabase();

const server = app.listen(process.env.API_PORT, () => {
    console.log(`Server started on PORT: ${process.env.API_PORT} in ${process.env.API_ENV} mode`);
})

// Handle Unhandle Promise rejections
process.on('unhandledRejection', err => {
    console.log(`ERROR: ${err.message}`);
    console.log('Shutting down the server due to Unhandled Promise rejection');
    server.close(() => {
        process.exit(1);
    })
})
