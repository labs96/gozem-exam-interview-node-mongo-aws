module.exports = {
    defaultServerResponse: {
      status: 200,
      message: '',
      body: {}
    },
    statusCode: {
        GET_OK: 200,
        CREATED: 201,
        NOT_CONTENT: 204,
        BAD_REQUEST: 400,
        UNAUTHORIZED: 401,
        FORBIDDEN: 403,
        NOT_FOUND: 404,
        DUPLICATE_EMAIL: 417,
        DUPLICATE_ENTRY: 418,
        UNPROCESSABLE: 422,
        INTERNAL_SERVER_ERROR: 500,
        BAD_GATEWAY: 502,
        SERVICE_UNAVAILABLE: 503,
    },
    message: {
        authentication: {
            SIGNUP_SUCCESS: 'Signup Success',
            LOGIN_SUCCESS: 'Login Success',
            DUPLICATE_EMAIL: 'User already exist with given email',
            USER_NOT_FOUND: 'User not found',
            USER_FOUND: 'User with this email already exist',
            INVALID_CREDENTIALS: 'Incorrect Email and Password',
            FORGOT_PASSWORD_SUCCESS: 'Otp Code successfully send to your Email',
            RESET_PASSWORD_SUCCESS: 'Your Password has been changed',
            INACTIVE_USER: 'The User is inactive',
        },
        userManagement: {
            UPDATE_INFORMATION_SUCCESS: '',
            UPDATE_PASSWORD_SUCCESS: '',
            UPDATE_PICTURE_SUCCESS: '',
            UPDATE_PREFERENCES_SUCCESS: '',
            CHANGE_TFA_STATUS_SUCCESS: '',
            CLEAR_SESSIONS_SUCCESS: '',
        },
        role: {
            ROLE_CREATED: 'Role Created Successfully',
            ROLE_FETCHED: 'Role Fetched Successfully',
            ROLE_UPDATED: 'Role Updated Successfully',
            ROLE_DELETED: 'Role Deleted Successfully',
            ROLE_NOT_FOUND: 'Role Not Found',
            ROLE_ATTACHED: 'Role Attached Successfully',
            ROLE_DETACHED: 'Role Detached Successfully',
        },
        category: {
            CATEGORY_CREATED: 'Category Created Successfully',
            CATEGORY_FETCHED: 'Category Fetched Successfully',
            CATEGORY_UPDATED: 'Category Updated Successfully',
            CATEGORY_DELETED: 'Category Deleted Successfully',
            CATEGORY_NOT_FOUND: 'Category Not Found'
        },
        requestValidation: {
            BAD_REQUEST: 'Invalid fields',
            TOKEN_MISSING: 'Token missing from header'
        },
        database: {
            INVALID_ID: 'Invalid Id'
        }
    }
}
